import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosStockPageRoutingModule } from './productos-stock-routing.module';

import { ProductosStockPage } from './productos-stock.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosStockPageRoutingModule
  ],
  declarations: [ProductosStockPage]
})
export class ProductosStockPageModule {}
