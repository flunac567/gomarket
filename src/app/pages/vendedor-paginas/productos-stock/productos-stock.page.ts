import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productos-stock',
  templateUrl: './productos-stock.page.html',
  styleUrls: ['./productos-stock.page.scss'],
})
export class ProductosStockPage implements OnInit {

  categorias= [
    {
      "categoria": "Frutas",
      "url":"/frutas-prod",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606185904/GoMarket/c-frutas_yjcowl.png"
    },
    {
      "categoria": "Verduras",
      "url":"/lacteos-prod",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606187003/GoMarket/c-verduras_is0n0s.jpg"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
