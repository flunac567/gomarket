import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosStockPage } from './productos-stock.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosStockPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosStockPageRoutingModule {}
