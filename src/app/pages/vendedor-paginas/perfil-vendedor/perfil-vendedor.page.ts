import { AutenticacionService } from './../../../services/autenticacion.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil-vendedor',
  templateUrl: './perfil-vendedor.page.html',
  styleUrls: ['./perfil-vendedor.page.scss'],
})
export class PerfilVendedorPage implements OnInit {

  constructor(private autenticacionService:AutenticacionService) { }

  ngOnInit() {
  }

  salir(){
    this.autenticacionService.logout()
  }

}
