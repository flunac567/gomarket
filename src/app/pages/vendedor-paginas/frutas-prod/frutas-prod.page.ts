import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-frutas-prod',
  templateUrl: './frutas-prod.page.html',
  styleUrls: ['./frutas-prod.page.scss'],
})
export class FrutasProdPage implements OnInit {

  frutas=[
    {
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016156/img-bBHh_763_itzfws.jpg", 
      "categoria": "Frutas",
      "nombre":"Piña Hawai",
      "Precio": "2.50"  ,
      "cantidad":"32kg"
    },
    {
        "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016689/img-DF721F8-_fvwmlv.jpg", 
        "categoria": "Frutas",
        "nombre":"Pera",
        "Precio": "1.20",
  "cantidad":"32kg"
    },
    {
        "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016356/img-mjISC16D_dfzrxn.jpg", 
        "categoria": "Frutas",
        "nombre":"Manzana Nacional",
        "Precio": "0.60",
          "cantidad":"32kg"
    },
    {
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016899/img-6pKMnTkX_tecvwz.jpg", 
      "categoria": "Frutas",
      "nombre":"Granadilla",
      "Precio": "2.9",
        "cantidad":"32kg"
  },
  {
    "img": "https://res.cloudinary.com/pbpc/image/upload/v1607017734/img-2wJirF5s_jog6uo.jpg", 
    "categoria": "Frutas",
    "nombre":"Cocona",
    "Precio": "2.50",
      "cantidad":"32kg"
  }
  ]
  constructor() { }

  ngOnInit() {
  }

}
