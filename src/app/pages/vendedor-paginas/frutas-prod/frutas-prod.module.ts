import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrutasProdPageRoutingModule } from './frutas-prod-routing.module';

import { FrutasProdPage } from './frutas-prod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    FrutasProdPageRoutingModule
  ],
  declarations: [FrutasProdPage]
})
export class FrutasProdPageModule {}
