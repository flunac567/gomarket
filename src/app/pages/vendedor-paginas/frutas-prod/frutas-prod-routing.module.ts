import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrutasProdPage } from './frutas-prod.page';

const routes: Routes = [
  {
    path: '',
    component: FrutasProdPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrutasProdPageRoutingModule {}
