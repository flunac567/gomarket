import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-verificar-pedido',
  templateUrl: './verificar-pedido.page.html',
  styleUrls: ['./verificar-pedido.page.scss'],
})
export class VerificarPedidoPage implements OnInit {
  //Guardar valor del QR
  scannedCode=null;

  constructor(private barcodeScanner:BarcodeScanner,private route:Router) { }

  ngOnInit() {
  }

  //Scannear codigo QR
  scanearQR(){
    this.barcodeScanner.scan().then(
      barcodeData=>{
        this.scannedCode=barcodeData.text;
      }
    );
    this.route.navigate(['/detalle-pedido'])
 }

}
