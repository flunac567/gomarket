import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      {
        path:'pedidos',
        loadChildren:() => import('./../pedidos/pedidos.module').then( m => m.PedidosPageModule)
      },
      {
        path:'productos-stock',
        loadChildren:() => import('./../productos-stock/productos-stock.module').then( m => m.ProductosStockPageModule)
      },
      {
        path:'perfil-vendedor',
        loadChildren:() => import('./../perfil-vendedor/perfil-vendedor.module').then( m => m.PerfilVendedorPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
