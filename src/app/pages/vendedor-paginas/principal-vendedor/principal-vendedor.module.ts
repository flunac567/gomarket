import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrincipalVendedorPageRoutingModule } from './principal-vendedor-routing.module';

import { PrincipalVendedorPage } from './principal-vendedor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PrincipalVendedorPageRoutingModule
  ],
  declarations: [PrincipalVendedorPage]
})
export class PrincipalVendedorPageModule {}
