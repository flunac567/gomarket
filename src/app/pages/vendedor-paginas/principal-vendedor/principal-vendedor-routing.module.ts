import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrincipalVendedorPage } from './principal-vendedor.page';

const routes: Routes = [
  {
    path: '',
    component: PrincipalVendedorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrincipalVendedorPageRoutingModule {}
