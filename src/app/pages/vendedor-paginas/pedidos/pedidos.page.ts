import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  clientes= [
    {
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607033447/hgie9mdxjdmfr1epwwsx.jpg",
      "nombreUsuario": "Eduardo",
      "monto":12.60
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607034065/aibrjlk3tmpyvrgwhmie.jpg",
      "nombreUsuario": "Rene Melchor",
      "monto":25.80
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607035314/zvvpm5w7k1we46qbb5lv.jpg",
      "nombreUsuario": "Jancarlo Roberto",
      "monto":75.20
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607035595/o1yrbyoxvq7clb9uy0jo.jpg",
      "nombreUsuario": "Fabiola Polet",
      "monto":10.50
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607035736/ataduggltjo0mxhwx6qj.jpg",
      "nombreUsuario": "Angelica Milagros",
      "monto":196.40
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607035860/mteipytcue7hpx2rvqvc.jpg",
      "nombreUsuario": "Edilberto ",
      "monto":203.80
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607035970/rttez6t8g705kwjo2w2f.jpg",
      "nombreUsuario": "David Cristan",
      "monto":33.90
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607036092/xin3ufdp1ik0bh6z2wk6.jpg",
      "nombreUsuario": "Nancy Maribel",
      "monto":17.30
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607036347/uwin7wiiygcjgruwjenj.jpg",
      "nombreUsuario": "Nelson Alex",
      "monto":85.20
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607036503/ee94d2uyvugnkaxwtqr6.jpg",
      "nombreUsuario": "Marlheni",
      "monto":360.40
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607037057/legtvrl6pzm6hwmpfhp8.jpg",
      "nombreUsuario": "Jaime",
      "monto":75.90
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607037131/mqhse7xwduog06fm2wty.jpg",
      "nombreUsuario": "Sandra Paola",
      "monto":124.80
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607037207/bnculxoydd75avm3v7vt.jpg",
      "nombreUsuario": "Daniel Felipe",
      "monto":32.40
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607037282/arudqxbnkseg6wbydkv7.jpg",
      "nombreUsuario": "Milagros",
      "monto":85.10
    },{
      "imgPerfil": "https://res.cloudinary.com/pbpc/image/upload/v1607037356/imkz3usv7e49k0rmhkd5.jpg",
      "nombreUsuario": "Luz Haydee",
      "monto":94.30
    },
  ]
  constructor() { }

  ngOnInit() {
  }

}
