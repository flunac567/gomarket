import { Component, OnInit } from '@angular/core';
import { MenuController } from "@ionic/angular";

@Component({
  selector: 'app-seleccion',
  templateUrl: './seleccion.page.html',
  styleUrls: ['./seleccion.page.scss'],
})
export class SeleccionPage implements OnInit {

  constructor(public menuCtrl:MenuController) {
    this.menuCtrl.swipeGesture(false);
    
   }

  ngOnInit() {
  }

}
