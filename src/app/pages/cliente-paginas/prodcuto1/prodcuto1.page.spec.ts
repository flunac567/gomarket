import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Prodcuto1Page } from './prodcuto1.page';

describe('Prodcuto1Page', () => {
  let component: Prodcuto1Page;
  let fixture: ComponentFixture<Prodcuto1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prodcuto1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Prodcuto1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
