import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prodcuto1',
  templateUrl: './prodcuto1.page.html',
  styleUrls: ['./prodcuto1.page.scss'],
})
export class Prodcuto1Page implements OnInit {

  cantidad=0
  a=false
  precio=2.50
  total=0
  constructor() { }

  ngOnInit() {
  }
select( ) {
      this.cantidad = ++this.cantidad;
      if (this.cantidad === 0) {
        this.a = false;
      } else {
        this.a = true;
        this.total=this.precio*this.cantidad
      }
    }

    deselect( ) {
      this.cantidad = --this.cantidad;
      if (this.cantidad === 0) {
        this.a = false;
      } else {
        this.a = true;
        this.total=this.precio/this.cantidad
      }
    }
}
