import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Prodcuto1PageRoutingModule } from './prodcuto1-routing.module';

import { Prodcuto1Page } from './prodcuto1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Prodcuto1PageRoutingModule
  ],
  declarations: [Prodcuto1Page]
})
export class Prodcuto1PageModule {}
