import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Prodcuto1Page } from './prodcuto1.page';

const routes: Routes = [
  {
    path: '',
    component: Prodcuto1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Prodcuto1PageRoutingModule {}
