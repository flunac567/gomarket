import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.page.html',
  styleUrls: ['./reservas.page.scss'],
})
export class ReservasPage implements OnInit {

  reservas= [
    {
      "vendedor":"Rosmery",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606959800/vghftcynigu5j8x4yg3y.jpg",
      "url":"/detalle-reserva"
    },
    {
      "vendedor":"Claudia",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606930855/wd1dn3kpqt8gqohuphkj.jpg",
      "url":"/detalle-reserva"
    },
    {
      "vendedor":"Luz Marina",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606931209/ntgrhejljtbjjhv4p8ky.jpg",
      "url":"/detalle-reserva"
    },
    {
      "vendedor":"Jesus Jassen",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606931457/z3kc8wagfezlrkumouxg.jpg",
      "url":"/detalle-reserva"
    },
    {
      "vendedor":"Benigna",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606933044/siknzkiypwlfrevcxzwc.jpg",
      "url":"/detalle-reserva"
    },
    {
      "vendedor":"Luis Alberto",
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1606958631/qpi4hnzxytkguhm7rdfr.jpg",
      "url":"/detalle-reserva"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
