import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalle-reserva',
  templateUrl: './detalle-reserva.page.html',
  styleUrls: ['./detalle-reserva.page.scss'],
})
export class DetalleReservaPage implements OnInit {

  detalle= [
    {
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016156/img-bBHh_763_itzfws.jpg", 
      "categoria": "Frutas",
      "nombre":"Piña",
      "Precio": "5.0"
    },
    {
      "img": "https://res.cloudinary.com/pbpc/image/upload/v1607016689/img-DF721F8-_fvwmlv.jpg", 
      "categoria": "Frutas",
      "nombre":"Pera",
      "Precio": "2.40"
  }
  ]
  constructor() { }

  ngOnInit() {
  }

}
