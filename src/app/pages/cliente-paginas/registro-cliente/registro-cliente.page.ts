import { AutenticacionService } from './../../../services/autenticacion.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registro-cliente',
  templateUrl: './registro-cliente.page.html',
  styleUrls: ['./registro-cliente.page.scss'],
})
export class RegistroClientePage implements OnInit {

  
  credentialsForm: FormGroup;

  constructor(private formBuilder: FormBuilder,private autenticacionService:AutenticacionService) { }
  ngOnInit() {
    this.credentialsForm=this.formBuilder.group({
      nombreUsuario:['',[Validators.required]],
      apellidoUsuario:['',[Validators.required]],
      correo:['',[Validators.required,Validators.email]],
      contraseña:['',[Validators.required,Validators.maxLength(100)]]
    })
  }

  registro(){
    this.autenticacionService.registro(this.credentialsForm.value).subscribe(res=>{
      this.autenticacionService.login(this.credentialsForm.value).subscribe();
    })
  }

}
