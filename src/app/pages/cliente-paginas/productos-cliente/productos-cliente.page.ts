import { DataService } from './../../../services/data.service';
import { Observable } from 'rxjs';
import { ProductoService } from './../../../services/producto.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment} from '@ionic/angular';

@Component({
  selector: 'app-productos-cliente',
  templateUrl: './productos-cliente.page.html',
  styleUrls: ['./productos-cliente.page.scss'],
})
export class ProductosClientePage implements OnInit {

  @ViewChild(IonSegment,{static:true}) segment:IonSegment;
  
  masProductos: number;
  menosProductos:number;
  textoBuscar='';

  productos: Observable<any>;

  categoria='';
  cantidad=0;
  a=false;

  constructor(private dataService:DataService) { 
    this.masProductos=0;
    this.menosProductos=0;
  }

  ngOnInit() {
    this.segment.value='Todas';
    this.productos = this.dataService.getProductosCliente();

  }

  segmentChanged( event){
    const valorSegmento=event.srcElement.value;

   if(valorSegmento==='Todas'){
     this.categoria='';
     return;
   }

   this.categoria=valorSegmento;

    console.log(valorSegmento);
  }

  agregar(){
      this.masProductos++;
  }

  select(idProducto) {
    this.cantidad = ++this.cantidad;
    if (this.cantidad === 0) {
      this.a = false;
    } else {
      this.a = true;
    }
  }
  buscar(event){
    console.log(event);
    this.textoBuscar=event.detail.value;
  }
}
