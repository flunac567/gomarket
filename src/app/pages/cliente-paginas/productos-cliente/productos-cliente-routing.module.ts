import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosClientePage } from './productos-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosClientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosClientePageRoutingModule {}
