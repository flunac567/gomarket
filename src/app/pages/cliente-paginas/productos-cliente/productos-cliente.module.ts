import { PipesModule } from './../../../pipes/pipes.module';
import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosClientePageRoutingModule } from './productos-cliente-routing.module';

import { ProductosClientePage } from './productos-cliente.page';

@NgModule({
 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PipesModule,
    ProductosClientePageRoutingModule
  ],
  declarations: [ProductosClientePage]
})
export class ProductosClientePageModule {}
