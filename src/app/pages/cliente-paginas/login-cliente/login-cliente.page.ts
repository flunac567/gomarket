import { MenuController } from '@ionic/angular';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-cliente',
  templateUrl: './login-cliente.page.html',
  styleUrls: ['./login-cliente.page.scss'],
})
export class LoginClientePage implements OnInit {

  constructor(private menuCtrl:MenuController) {
    this.menuCtrl.enable(false,'cliente-menu')
   }

  ngOnInit() {
  }

}
