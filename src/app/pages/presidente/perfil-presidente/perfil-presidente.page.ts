import { AutenticacionService } from './../../../services/autenticacion.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil-presidente',
  templateUrl: './perfil-presidente.page.html',
  styleUrls: ['./perfil-presidente.page.scss'],
})
export class PerfilPresidentePage implements OnInit {

  constructor(private autenticacionService:AutenticacionService) { }

  ngOnInit() {
  }

  salir(){
    this.autenticacionService.logout()
  }

}
