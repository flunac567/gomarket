import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilPresidentePageRoutingModule } from './perfil-presidente-routing.module';

import { PerfilPresidentePage } from './perfil-presidente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilPresidentePageRoutingModule
  ],
  declarations: [PerfilPresidentePage]
})
export class PerfilPresidentePageModule {}
