import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilPresidentePage } from './perfil-presidente.page';

const routes: Routes = [
  {
    path: '',
    component: PerfilPresidentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilPresidentePageRoutingModule {}
