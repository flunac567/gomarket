import { PipesModule } from './../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresidentePrincipalPageRoutingModule } from './presidente-principal-routing.module';

import { PresidentePrincipalPage } from './presidente-principal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    PresidentePrincipalPageRoutingModule
  ],
  declarations: [PresidentePrincipalPage]
})
export class PresidentePrincipalPageModule {}
