import { PuestoService } from './../../../services/puesto.service';
import { AutenticacionService } from './../../../services/autenticacion.service';
import { DataService } from './../../../services/data.service';
import { Observable } from 'rxjs';
import { IonSegment } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-presidente-principal',
  templateUrl: './presidente-principal.page.html',
  styleUrls: ['./presidente-principal.page.scss'],
})
export class PresidentePrincipalPage implements OnInit {

  @ViewChild(IonSegment,{static:true}) segment:IonSegment;

  vendedores: Observable<any>;

  tipo='';
  constructor(private puestoService:PuestoService,private autentitacionService:AutenticacionService) { }

  ngOnInit() {
    this.segment.value='Vendedores';
    this.vendedores=this.puestoService.getPuestos();
  }

  segmentChanged(event){
    const valorSegmento=event.srcElement.value;

   if(valorSegmento==='Vendedores'){
     this.tipo='';
     return;
   }

   this.tipo=valorSegmento;

    console.log(valorSegmento);
  }

  logout(){
       this.autentitacionService.logout();
  }
}

