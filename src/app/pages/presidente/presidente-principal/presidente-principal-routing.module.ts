import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresidentePrincipalPage } from './presidente-principal.page';

const routes: Routes = [
  {
    path: '',
    component: PresidentePrincipalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresidentePrincipalPageRoutingModule {}
