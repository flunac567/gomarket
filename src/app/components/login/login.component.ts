import { AutenticacionBuenoService } from './../../services/autenticacion-bueno.service';
import { AutenticacionService } from './../../services/autenticacion.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  
  credentialsForm: FormGroup;
  usuario=''

  constructor( private formBuilder: FormBuilder,
    private AutenticacionService:AutenticacionService) { }

  ngOnInit() {
    this.credentialsForm=this.formBuilder.group({
      correo:['',[Validators.required,Validators.email]],
      contraseña:['',[Validators.required,Validators.maxLength(100)]]
    })
  }

  onSubmit(){
    this.AutenticacionService.login(this.credentialsForm.value)
    .subscribe();
  }

}
