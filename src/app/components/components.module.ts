import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuheaderClienteComponent } from './cliente/menuheader-cliente/menuheader-cliente.component';
import { LogoComponent } from './logo/logo.component';
import { HeaderBackComponent } from './header-back/header-back.component';
import { HeaderVendedorComponent } from './vendedor/header-vendedor/header-vendedor.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { VendedoresComponent } from './cliente/vendedores/vendedores.component';
import { ProductosComponent } from './cliente/productos/productos.component';
import { ClientesComponent } from './vendedor/clientes/clientes.component';
import { MiscategoriasComponent } from './vendedor/miscategorias/miscategorias.component';
import { MisproductosComponent } from './vendedor/misproductos/misproductos.component';
import { RegistroComponent } from './registro/registro.component';
import { CategoriasdisponiblesComponent } from './cliente/categoriasdisponibles/categoriasdisponibles.component';



@NgModule({
  declarations: [MenuheaderClienteComponent,LogoComponent, HeaderBackComponent, HeaderVendedorComponent, MenuComponent, LoginComponent, VendedoresComponent, ProductosComponent, ClientesComponent, MiscategoriasComponent, MisproductosComponent, RegistroComponent, CategoriasdisponiblesComponent],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [MenuheaderClienteComponent,LogoComponent, HeaderBackComponent, HeaderVendedorComponent, MenuComponent, LoginComponent, VendedoresComponent, ProductosComponent, ClientesComponent, MiscategoriasComponent, MisproductosComponent, RegistroComponent, CategoriasdisponiblesComponent]
})
export class ComponentsModule { }
