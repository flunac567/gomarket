import { AutenticacionBuenoService } from './../../services/autenticacion-bueno.service';
import { CategoriasPageRoutingModule } from './../../pages/cliente-paginas/categorias/categorias-routing.module';
import { usuarioInt } from './../../interfaces/usuario.interface';
import { AutenticacionService } from './../../services/autenticacion.service';
import { DataService } from './../../services/data.service';
import { Router, RouterEvent } from '@angular/router';
import { Menu } from '../../interfaces/menu.interface';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthPruebaService } from "../../services/auth-prueba.service";
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  
 opciones: Observable<Menu[]>;
 selectedPath='';
 datos: Observable<usuarioInt[]>;
 usuario:any
 temaOscuro: boolean=true;
 
 menuCliente=this.dataService.getMenuCliente();
 menuVendedor=this.dataService.getMenuVendedor();
 //usuario= this.autenticacionService.logueado();
 //seleccionar ruta
 constructor(private router: Router,private dataService:DataService,private autenticacionService:AutenticacionService) {
   this.router.events.subscribe((event:RouterEvent)=> {
     this.selectedPath= event.url;
   })  
   
   //this.datosDelUsuario();
  }
  
  //Activar Modo Oscuro
   activarTemOscuro(){
     this.temaOscuro= !this.temaOscuro;
     document.body.classList.toggle('dark');
   }
 
   ngOnInit() {
   
    this.opciones=this.menuCliente;

   }

   salir(){
     this.autenticacionService.logout();
   }
 
}
