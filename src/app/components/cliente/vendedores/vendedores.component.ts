import { PuestoService } from './../../../services/puesto.service';
import { Observable } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { PuestoCategoria } from 'src/app/interfaces/puestoCategoria.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.scss'],
})
export class VendedoresComponent implements OnInit {

  puestos:Observable<PuestoCategoria[]>
  categoriaId=null

  constructor(private puestoService:PuestoService,private activeRoute:ActivatedRoute) { }
    
  ngOnInit() {
    this.categoriaId=this.activeRoute.snapshot.paramMap.get('categoriaId')
    console.log(this.categoriaId)
    //this.categoriaId=this.puestoService.getId()
    
    this.puestos=this.puestoService.getPuestosPorCategorias(this.categoriaId)
    console.log(this.puestos)
    //console.log(this.puestoService.getPuestosPorCategorias(this.categoriaId))
  }

}
