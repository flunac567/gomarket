import { Router } from '@angular/router';
import { PuestoService } from './../../../services/puesto.service';
import { CategoriaService } from './../../../services/categoria.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/interfaces/categoria.interface';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-categoriasdisponibles',
  templateUrl: './categoriasdisponibles.component.html',
  styleUrls: ['./categoriasdisponibles.component.scss'],
})
export class CategoriasdisponiblesComponent implements OnInit {

  categorias: Observable<Categoria[]>;
  constructor(private categoriaService:CategoriaService,private navCtrl:NavController,private route:Router) { }

  ngOnInit() {
    this.categorias=this.categoriaService.getCategoriasCliente()
    console.log(this.categorias)
  }
   
  verCategoria(categoria){
    this.route.navigate(['puestos',categoria])
    //console.log(categoria)
     //this.route.navigate(['puestos'],{categoriaId:this.categoriaCodigo})
     //console.log(categoriaId)
  }
}
