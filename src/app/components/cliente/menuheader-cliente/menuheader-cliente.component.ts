import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-menuheader-cliente',
  templateUrl: './menuheader-cliente.component.html',
  styleUrls: ['./menuheader-cliente.component.scss'],
})
export class MenuheaderClienteComponent implements OnInit {
  
  //Nombre de la pagina
  @Input() titulo: string;
  @Input() menuId: string;

  constructor() { }

  ngOnInit() {}

}
