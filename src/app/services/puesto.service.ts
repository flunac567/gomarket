import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Puestos } from '../interfaces/puestos.interface';
import { PuestoCategoria } from '../interfaces/puestoCategoria.interface';

@Injectable({
  providedIn: 'root'
})
export class PuestoService {

  URL=environment.url
  categoriaId

  constructor(private http:HttpClient) { }
   //categoriaId=null
  //Obtener todos los puestos 
  getPuestos(){
    return this.http.get<Puestos[]>(`${this.URL}/api/puesto/`);
  }

  //Obtener Puestos por Categorias
  getPuestosPorCategorias(categoria){
    this.categoriaId=categoria
    console.log(`${this.URL}/api/puesto/${this.categoriaId}`)
    return this.http.get<PuestoCategoria[]>(`${this.URL}/api/puesto/${this.categoriaId}`);
  }


}
