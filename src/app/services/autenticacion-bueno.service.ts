import { AlertController } from '@ionic/angular';
import { catchError, tap } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

//TOKEN DE AUTENTICACION
const TOKEN_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})

export class AutenticacionBuenoService {

  URL=environment.url
  usuario=''

  constructor(private http:HttpClient,private alert: AlertController,private storage:Storage
    ,private help:JwtHelperService,private router:Router) { }
 
  //Login
  login(crendeciales){
    return this.http.post(`${this.URL}/api/autenticacion/loguearse`,crendeciales).pipe(
      tap(res=>{
       console.log(res)
        this.storage.set(TOKEN_KEY, res['token']);//Guardamos el token
        this.usuario = this.help.decodeToken(res['token']);//Obtener informacion del token
        
        const rol=this.usuario['rolId']//Rol del usuario
        this.rolScreen(rol)
      }),
      catchError(e => {
        this.mostrarAlerta(e.error.mensaje);
        throw new Error(e);
      })
    );
  }

  //Mostrar mensaje de error  
  mostrarAlerta(mensaje) {
    let alert = this.alert.create({
      message: mensaje,
      header: 'Error',
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }

  //Ruta segun su rol
  rolScreen(rol){
     //Enviar a cada usuario a su pantalla respectiva segun su rol
     if(rol=== 'Cliente'){
      this.router.navigate(['/categorias'])
    }else if(rol==='Vendedor'){
      this.router.navigate(['/tabs/pedidos'])
    }else if(rol==='Presidente'){
      this.router.navigate(['/presidente-principal'])
    }
  }
  
  //Revisamos si el usuario esta logueado
  estaLogueado(){
    //!!this.storage.get(TOKEN_KEY)
    return !!this.storage.get(TOKEN_KEY).then(token => {
      console.log('Token del usuario: ',token);
    });
  }
  //Obtener token
  obtenerToken(){
    return this.storage.get(TOKEN_KEY)
  }

  salir(){
    this.storage.remove(TOKEN_KEY).then(() => {
      this.router.navigate(['/login-cliente'])
    });
  }
}
