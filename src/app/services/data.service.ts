import { Menu } from '../interfaces/menu.interface';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient,HttpErrorResponse,HttpHeaders } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class DataService {

  URL=environment.url;

  constructor(private http: HttpClient) { }
   
  //Menu para Cliente
  getMenuCliente(){
    return this.http.get<Menu[]>('/assets/data/menu-cliente.json');
  }

  //Menu para Vendedor
  getMenuVendedor(){
    return this.http.get<Menu[]>('/assets/data/menu-vendedor.json');
  }

  getProductosCliente(){
    return this.http.get('/assets/data/productos-cliente.json');
  }

  getVendedores(){
    return this.http.get('/assets/data/vendedor-frutas.json');
  }

  getVendedoresPresi(){
    return this.http.get('/assets/data/vendedor-frutas.json');
  }
}
