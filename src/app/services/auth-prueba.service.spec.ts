import { TestBed } from '@angular/core/testing';

import { AuthPruebaService } from './auth-prueba.service';

describe('AuthPruebaService', () => {
  let service: AuthPruebaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthPruebaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
