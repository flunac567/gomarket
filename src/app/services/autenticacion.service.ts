import { Router } from '@angular/router';
import { usuarioInt } from './../interfaces/usuario.interface';

import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpHeaders } from "@angular/common/http";
import { AlertController, Platform } from '@ionic/angular';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';
import { Storage } from '@ionic/storage';
import { map } from "rxjs/operators";
import { RequestOptions} from '@angular/http';

//TOKEN DE AUTENTICACION
const TOKEN_KEY = 'access_token';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {
   
   //DATOS DE AUTENTICACION
   URL=environment.url;
   usuario=null;
   authenticationState = new BehaviorSubject(false);

  constructor(
    private http:HttpClient,
    private help:JwtHelperService,
    private storage:Storage,
    private platform: Platform,
    private alert:AlertController,
    private router:Router) {
    //Validacion de autenticacion al iniciar la app
    this.platform.ready().then(()=>{
      this.verificarToken();
    }); 
  }
  
  //VERIFICACION DE TOKEN
  verificarToken(){
    this.storage.get(TOKEN_KEY).then(token => {
      console.log('usuario activo: ',token);
      if (token) {
        //let decodicifar = this.help.decodeToken(token);
        let estaExpirado = this.help.isTokenExpired(token);
        if (!estaExpirado) {
          //this.usuario = decodicifar;
          this.authenticationState.next(true);
        } else {
          this.storage.remove(TOKEN_KEY);
        }
      }
    });
  }
  
  //REGISTRO DE USUARIOS
  registro(credenciales){
    return this.http.post<any>(this.URL+ '/api/autenticacion/registrarse',credenciales).pipe(
      catchError(e => {
        this.mostrarAlerta(e.error.message);
        throw new Error(e);
      })
    );
  }
  
  //LOGIN
  login(credenciales) {
    return this.http.post(`${this.URL}/api/autenticacion/loguearse`, credenciales)
      .pipe(
        tap(res => {
          //localStorage.setItem(TOKEN_KEY, res['token'])
          this.storage.set(TOKEN_KEY, res['token']);//Guardamos el token
          this.usuario = this.help.decodeToken(res['token']);
          this.authenticationState.next(true);//le damos su estado al usuario
        }),
        catchError(e => {
          this.mostrarAlerta(e.error.message);
          throw new Error(e);
        })
      );
  }
  
  //CERRAR SESION
  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false)
    });
  }
  
  //SI EL USUARIO ESTA LOGUEADO
  logueado(){
    return this.http.get<usuarioInt[]>(`${this.URL}/api/usuarios/datos`).pipe(

      map(resp=> resp['data'])
    );

  }
  
  //VERIFICAR AUTENTICACION
  estaAutenticado() {
    this.authenticationState.value;
    return ;
  }
  
   //Ruta segun su rol
   rolScreen(rol){
    //Enviar a cada usuario a su pantalla respectiva segun su rol
    if(rol=== 'Cliente'){
     this.router.navigate(['/categorias'])
   }else if(rol==='Vendedor'){
     this.router.navigate(['/tabs/pedidos'])
   }else if(rol==='Presidente'){
     this.router.navigate(['/presidente-principal'])
   }
 }

  //Mostrar mensaje de error  
  mostrarAlerta(message) {
    let alert = this.alert.create({
      message: message,
      header: 'Error',
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }

  redirigir(){
      this.storage.get(TOKEN_KEY).then(token=>{
        this.usuario = this.help.decodeToken(token);
        console.log(this.usuario);
        const rol=this.usuario['rolId']//Rol del usuario
        this.rolScreen(rol)
      })
  }
}
