import { TestBed } from '@angular/core/testing';

import { AutenticacionBuenoService } from './autenticacion-bueno.service';

describe('AutenticacionBuenoService', () => {
  let service: AutenticacionBuenoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutenticacionBuenoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
