import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categoria } from '../interfaces/categoria.interface';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  //DATOS DE AUTENTICACION
  URL=environment.url;
  constructor(private http:HttpClient) { }

  getCategoriasCliente(){
    return this.http.get<Categoria[]>(`${this.URL}/api/categorias/categoriascliente/`);
  }


}
