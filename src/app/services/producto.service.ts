import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) { }
  

  getProductosCliente(){
    return this.http.get('/assets/data/productos-cliente.json');
  }

}
