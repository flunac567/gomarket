import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { filter } from 'rxjs/operators';

const TOKEN_KEY= 'user-access-token';

@Injectable({
  providedIn: 'root'
})
export class AuthPruebaService {

  //Datos
  user: Observable<any>;
  authState = new BehaviorSubject(null);
  constructor(private storage:Storage,private router:Router) { 
     //Cargamos el usuario activo
     this.loadUser()
     //observamos el estado del usuario
     this.user= this.authState.asObservable().pipe(
       filter(response=>response)
     );
  }


  loadUser(){
    this.storage.get(TOKEN_KEY).then(data=>{
      console.log('usuario activo: ', data);
      if(data){
        this.authState.next(data);
      }else{
        this.authState.next({email: null,role:null});
      }
    })
  }
  
  //Login
  login(credentials){
      let email= credentials.email;
      let password=credentials.password;
      let user= null;

      user= {email,role:'Cliente'};
      
      if(email === 'pepeal@gmail.com' && password ==='micontraseña'){
        user= {email,role:'Presidente'};
      } else if(email==='flunac567@gmail.com' && password==='micontraseña'){
        user= {email,role:'Vendedor'};
      } else if(email==='marcos@gmail.com' && password ==='micontraseña'){
        user= {email,role:'Cliente'}
      }
    
      
      this.authState.next(user);//le damos su estado a usuario
      this.storage.set(TOKEN_KEY,user);//GUardamos su token en el local storage

      //retornamos el usuario con todo y su estado
      return of(user);
  }

  //Salir
  async logout(){
    await this.storage.set(TOKEN_KEY,null);
    this.authState.next(null);
    this.router.navigateByUrl('/seleccion');
  }

}
