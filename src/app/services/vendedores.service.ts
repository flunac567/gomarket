import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { vendedores } from '../interfaces/vendedor.interface';
@Injectable({
  providedIn: 'root'
})
export class VendedoresService {

  URL=environment.url

  constructor(private http: HttpClient) { }

  //Obtener todos los vendedores
  getVendedores(){
    return this.http.get<vendedores[]>(`${this.URL}/api/puesto/`);
  }
}
