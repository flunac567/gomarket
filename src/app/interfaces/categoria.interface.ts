export interface Categoria {
    _id:             String;
    nombreCategoria: String;
    createdAt:       Date;
    updatedAt:       Date;
    imgCategoria:    String;
}