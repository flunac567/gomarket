export interface detalleVendedor {
    _id:       String;
    imgPuesto: String;
    nombre:    String;
    apellido:  String;
    correo:    String;
    edad:      String;
    genero:    String;
    nroPuesto: String;
    estado:    String;
    color:     String;
    categoria: String[];
}