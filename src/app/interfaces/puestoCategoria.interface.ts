export interface PuestoCategoria {
    _id:       String;
    imgPuesto: String;
    nombre:    String;
    nroPuesto: String;
    estado:    String;
    color:     String;
    categoria: String;
}