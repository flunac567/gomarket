export interface Menu{
    icon: String, 
    name: String,
    url: String
}