export interface vendedores {
    estado:          String;
    rolId:           String;
    categoriaId:     String;
    imgPerfil:       String;
    nombreUsuario:   String;
    apellidoUsuario: String;
    correo:          String;
    edad:            String;
    genero:          String;
    contraseña:      String;
    imgPerfilId:     String;
    nroPuesto:       String;
}