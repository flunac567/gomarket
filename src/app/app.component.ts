import { Router } from '@angular/router';
import { AutenticacionService } from './services/autenticacion.service';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from './services/data.service';

import { Menu } from './interfaces/menu.interface';
import { usuarioInt } from './interfaces/usuario.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})


export class AppComponent {

 
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private autenticacionService:AutenticacionService,
    private router:Router
  ) {
    this.initializeApp();
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
       
      //Estado de autenticacion
      
      this.autenticacionService.authenticationState.subscribe(state=>{
        if(state){
          //this.router.navigate(['categorias']);
          this.autenticacionService.redirigir();
        }else{
          this.router.navigate(['login-cliente']);
        }
      }) 


    });
  }

}
