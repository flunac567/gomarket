import { AutenticacionBuenoService } from './../services/autenticacion-bueno.service';
import { Storage } from '@ionic/storage';
import { catchError, mergeMap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';


const TOKEN_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})
export class InterceptorHeaderService implements HttpInterceptor {



  constructor( private AutenticacionService:AutenticacionBuenoService) { }
 
  intercept(req:HttpRequest<any>, next:HttpHandler):Observable<HttpEvent <any>>{
  
    
    const tokenReq=req.clone({
      setHeaders:{
        Autorization:`Bearer ${this.AutenticacionService.obtenerToken()}`
      }
    })
     console.log(this.AutenticacionService.obtenerToken());

     return next.handle(tokenReq).pipe(
       catchError(this.manejarError)
     )
   
  }

  manejarError(error: HttpErrorResponse){
    console.log('Sucedio un error');
    console.log('Registor en el archivo de log de los files');
    console.warn(error)

    return throwError('Error personalizado');
  }

}
