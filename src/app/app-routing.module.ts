import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'reservas',
    pathMatch: 'full'
  },
  {
    path: 'registro-cliente',
    loadChildren: () => import('./pages/cliente-paginas/registro-cliente/registro-cliente.module').then( m => m.RegistroClientePageModule)
  },
  {
    path: 'login-vendedor',
    loadChildren: () => import('./pages/vendedor-paginas/login-vendedor/login-vendedor.module').then( m => m.LoginVendedorPageModule)
  },
  {
    path: 'registro-vendedor',
    loadChildren: () => import('./pages/vendedor-paginas/registro-vendedor/registro-vendedor.module').then( m => m.RegistroVendedorPageModule)
  },
  {
    path: 'principal-vendedor',
    loadChildren: () => import('./pages/vendedor-paginas/principal-vendedor/principal-vendedor.module').then( m => m.PrincipalVendedorPageModule)
  },
  {
    path: 'categorias',
    loadChildren: () => import('./pages/cliente-paginas/categorias/categorias.module').then( m => m.CategoriasPageModule)
  },
  {
    path: 'seleccion',
    loadChildren: () => import('./pages/seleccion/seleccion.module').then( m => m.SeleccionPageModule)
  },
  {
    path: 'reservas',
    loadChildren: () => import('./pages/cliente-paginas/reservas/reservas.module').then( m => m.ReservasPageModule)
  },
  {
    path: 'pedidos',
    loadChildren: () => import('./pages/vendedor-paginas/pedidos/pedidos.module').then( m => m.PedidosPageModule)
  },
  {
    path: 'puestos/:categoriaId',
    loadChildren: () => import('./pages/cliente-paginas/puestos/puestos.module').then( m => m.PuestosPageModule)
  },
  {
    path: 'perfil-cliente',
    loadChildren: () => import('./pages/cliente-paginas/perfil-cliente/perfil-cliente.module').then( m => m.PerfilClientePageModule)
  },
  {
    path: 'productos-cliente',
    loadChildren: () => import('./pages/cliente-paginas/productos-cliente/productos-cliente.module').then( m => m.ProductosClientePageModule)
  },
  {
    path: 'detalle-reserva',
    loadChildren: () => import('./pages/cliente-paginas/detalle-reserva/detalle-reserva.module').then( m => m.DetalleReservaPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/vendedor-paginas/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'perfil-vendedor',
    loadChildren: () => import('./pages/vendedor-paginas/perfil-vendedor/perfil-vendedor.module').then( m => m.PerfilVendedorPageModule)
  },
  {
    path: 'productos-stock',
    loadChildren: () => import('./pages/vendedor-paginas/productos-stock/productos-stock.module').then( m => m.ProductosStockPageModule)
  },
  {
    path: 'frutas-prod',
    loadChildren: () => import('./pages/vendedor-paginas/frutas-prod/frutas-prod.module').then( m => m.FrutasProdPageModule)
  },
  {
    path: 'detalle-pedido',
    loadChildren: () => import('./pages/vendedor-paginas/detalle-pedido/detalle-pedido.module').then( m => m.DetallePedidoPageModule)
  },
  {
    path: 'presidente-principal',
    loadChildren: () => import('./pages/presidente/presidente-principal/presidente-principal.module').then( m => m.PresidentePrincipalPageModule)
  },
  {
    path: 'perfil-presidente',
    loadChildren: () => import('./pages/presidente/perfil-presidente/perfil-presidente.module').then( m => m.PerfilPresidentePageModule)
  },
  {
    path: 'design',
    loadChildren: () => import('./pages/design/design.module').then( m => m.DesignPageModule)
  },
  {
    path: 'prodcuto1',
    loadChildren: () => import('./pages/cliente-paginas/prodcuto1/prodcuto1.module').then( m => m.Prodcuto1PageModule)
  },
  {
    path: 'verificar-pedido',
    loadChildren: () => import('./pages/vendedor-paginas/verificar-pedido/verificar-pedido.module').then( m => m.VerificarPedidoPageModule)
  },
  {
    path: 'detalle-cliente',
    loadChildren: () => import('./pages/vendedor-paginas/detalle-cliente/detalle-cliente.module').then( m => m.DetalleClientePageModule)
  },
  {
    path: 'login-cliente',
    loadChildren: () => import('./pages/cliente-paginas/login-cliente/login-cliente.module').then( m => m.LoginClientePageModule)
  },

 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
